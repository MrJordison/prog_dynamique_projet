#ifndef _RACCORDEUR_REC_H
#define	_RACCORDEUR_REC_H

#include "raccordeur.h"
#include <vector>

/*!
 * Raccordeur simple : il coupe au milieu et calcule le cout du raccord comme
 * etant la somme des distances sur la zone de recouvrement.
 *
 * Cette classe est une classe concrete et derive/implemente le concept de
 * Raccordeur (\see{Raccordeur}).
 */
class RaccordeurRecursif : public Raccordeur {

  /*!
   * le calcul du raccord (\see{Raccordeur#calculerRaccord}).
   * Le code effectif se trouve dans RaccordeurSimple.cpp
   */
  virtual int calculerRaccord(MatInt2* distances, int* coupe);

  int coupe_optimale(MatInt2* distances, int i, int j, std::vector< std::vector<int> >& dyn_costs_tab, std::vector<std::vector<std::vector<int> > >& dyn_coupes_tab);

  virtual ~RaccordeurRecursif(); // destructeur

};

#endif	/* _RACCORDEUR_REC_NAIF_H */
