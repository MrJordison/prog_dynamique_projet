
#include <iostream>
#include <climits>
#include <algorithm>

#include "raccordeur_recursif.h"
#include "tools.h"
#include <vector>

using namespace std;

int RaccordeurRecursif::calculerRaccord(MatInt2* distances, int* coupe)
{
    //Récupération de la hauteur et largeur du tableau de distances
    int hauteur = distances->nLignes();
    int largeur = distances->nColonnes();

    int min = 0;
    //initialisation du tableau ou seront stockés les valeurs temporaires de couts générés par la fonction récursive
    vector<vector<int>> dyn_costs_tab = vector<vector<int>>(hauteur,vector<int>(largeur,-1));

    //initialisation des tableaux de coupes
    vector<vector<vector<int> > > dyn_coupes_tab = vector<vector<vector<int> > >(hauteur,vector<vector<int> >(largeur,vector<int>()));

    //appel de la fonction de coupe optimale pour chaque valeur de la dernière ligne du tableau de distances
    for(int i = 0 ; i < largeur ; ++i){
        dyn_costs_tab.at(hauteur-1).at(i) = coupe_optimale(distances,hauteur-1,i,dyn_costs_tab, dyn_coupes_tab);
        //regarde si le cout de la coupe optimale de la colonne en cours est le plus petit
        if(min!=0 && dyn_costs_tab.at(hauteur-1).at(i) < dyn_costs_tab.at(hauteur-1).at(min))
            min = i;
    }

    //stockage des valeurs de la coupe optimale dont le coût est le minimum dans le paramètre de retour coupe
    reverse(dyn_coupes_tab.at(hauteur-1).at(min).begin(),
        dyn_coupes_tab.at(hauteur-1).at(min).end());
    coupe = new int[hauteur];
    for(int i = 0 ; i < hauteur; ++i)
        coupe[i] = dyn_coupes_tab.at(hauteur-1).at(min).at(i);

    //retourne le coût minimal
    return dyn_costs_tab.at(hauteur-1).at(min);
}

int RaccordeurRecursif::coupe_optimale(MatInt2* distances, int i, int j, vector<vector<int>>& dyn_costs_tab, vector<vector<vector<int> > >& dyn_coupes_tab){

    //Cas d'arrêt récursivité dynamique : retourne le coût de la coupe optimale récursive si déjà existante
    if(dyn_costs_tab.at(i).at(j) !=-1){
        return dyn_costs_tab.at(i).at(j);
    }

    //Sinon...

    //Récupère dans le tableau de distance la valeur à l'emplacement (i,j)
    int cost = distances->get(i,j);
    //met à jour le tableau de coupe
    dyn_coupes_tab.at(i).at(j).push_back(j);

    //initialise le tableau ou seront stockés les coûts de coupe optimale des appels récursifs à j-1, j et j+1
    vector<int> recursive_costs = vector<int>(3, 0);

    //Si la ligne est la première du tableau de distance, on renvoie simplement la distance (i,j) ainsi que l'indice de colonne dans le tableau de coupe optimale
    if(i!=0){

        //si la colonne est la première et la dernière (cad recouvrement = 1 pixel) alors appel récursif seulement de j (j-1 et j+1 hors limite du tableau)
        if(j==0 && j==distances->nColonnes()-1){
            recursive_costs.at(0) = INT_MAX;
            recursive_costs.at(1) = coupe_optimale(distances,i-1,j,dyn_costs_tab,dyn_coupes_tab);
            recursive_costs.at(2) = INT_MAX;
        }
        //si la colonne est la première, on gère uniquement les appels récursifs de j et j+1 (j-1 est hors limite du tableau)
        else if(j == 0){
            recursive_costs.at(0) = INT_MAX;
            recursive_costs.at(1) = coupe_optimale(distances,i-1,j,dyn_costs_tab,dyn_coupes_tab);
            recursive_costs.at(2) = coupe_optimale(distances,i-1,j+1,dyn_costs_tab,dyn_coupes_tab);
        }
        //si la colonne est la dernière, on gère uniquement les appels récursifs de j-1 et j (j+1 est hors limite du tableau)
        else if(j == distances->nColonnes()-1){
            recursive_costs.at(0) = coupe_optimale(distances,i-1,j-1,dyn_costs_tab,dyn_coupes_tab);
            recursive_costs.at(1) = coupe_optimale(distances,i-1,j,dyn_costs_tab,dyn_coupes_tab);
            recursive_costs.at(2) = INT_MAX;
        }
        //sinon c'est un cas normal, on appelle récursivement sur j-1, j et j+1 la coupe optimale
        else{
            recursive_costs.at(0) = coupe_optimale(distances,i-1,j-1,dyn_costs_tab,dyn_coupes_tab);
            recursive_costs.at(1) = coupe_optimale(distances,i-1,j,dyn_costs_tab,dyn_coupes_tab);
            recursive_costs.at(2) = coupe_optimale(distances,i-1,j+1,dyn_costs_tab,dyn_coupes_tab);
        }

        //on récupère la valeur minimale de coût minimal des appels récursifs...
        int min = 0;
        if(recursive_costs.at(1) < recursive_costs.at(min))
            min = 1;
        if(recursive_costs.at(2) < recursive_costs.at(min))
            min = 2;

        //... ainsi que le tableau de coupe associé que l'on ajoute au tableau de coupe actuel
        int ind_coupe = (min == 0) ? (j-1) : (min==1) ? j : (j+1);
        dyn_coupes_tab.at(i).at(j).insert(dyn_coupes_tab.at(i).at(j).end(),
                                            dyn_coupes_tab.at(i-1).at(ind_coupe).begin(),
                                            dyn_coupes_tab.at(i-1).at(ind_coupe).end());

        //on ajoute à la distance (i,j) le cout minimum des appels récursifs
        cost+=recursive_costs.at(min);
        dyn_costs_tab.at(i).at(j) = cost;
    }
    //on retourne le cout
    return cost;
}

RaccordeurRecursif::~RaccordeurRecursif()
{
  // pas de ressources a liberer
}
