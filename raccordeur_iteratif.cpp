
#include <iostream>
#include <climits>
#include <algorithm>

#include "raccordeur_iteratif.h"
#include "tools.h"
#include <vector>

using namespace std;

int RaccordeurIteratif::calculerRaccord(MatInt2* distances, int* coupe)
{
    //Récupération de la hauteur et largeur du tableau de distances
    int hauteur = distances->nLignes();
    int largeur = distances->nColonnes();

    //initialisation du tableau ou seront stockés les valeurs temporaires de couts générés par la fonction récursive
    vector<vector<int>> dyn_costs_tab = vector<vector<int>>(hauteur,vector<int>(largeur,-1));

    //initialisation des tableaux de coupes
    vector<vector<vector<int> > > dyn_coupes_tab = vector<vector<vector<int> > >(hauteur,vector<vector<int> >(largeur,vector<int>()));

    int min;
    //appel de la fonction de coupe optimale pour chaque valeur de la dernière ligne du tableau de distances
    for(int i = 0 ; i < hauteur ; ++i){
        for(int j = 0 ; j < largeur ; ++j){
            dyn_coupes_tab.at(i).at(j).push_back(j);
            if(i==0)
                dyn_costs_tab.at(i).at(j) = distances->get(i,j);

            else{
                //si la colonne est la première et la dernière (cad recouvrement = 1 pixel) alors appel récursif seulement de j (j-1 et j+1 hors limite du tableau)
                if(j==0 && j==distances->nColonnes()-1){
                    min = j;
                }
                //si la colonne est la première, on gère uniquement les appels récursifs de j et j+1 (j-1 est hors limite du tableau)
                else if(j == 0){
                    if(dyn_costs_tab.at(i-1).at(j) < dyn_costs_tab.at(i-1).at(j+1))
                        min = j;
                    else
                        min = j+1;
                }
                //si la colonne est la dernière, on gère uniquement les appels récursifs de j-1 et j (j+1 est hors limite du tableau)
                else if(j == distances->nColonnes()-1){
                    if(dyn_costs_tab.at(i-1).at(j-1) < dyn_costs_tab.at(i-1).at(j))
                        min = j-1;
                    else
                        min = j;
                }
                //sinon c'est un cas normal, on appelle récursivement sur j-1, j et j+1 la coupe optimale
                else{
                    if(dyn_costs_tab.at(i-1).at(j-1) < dyn_costs_tab.at(i-1).at(j))
                        min = j-1;
                    else
                        min = j;
                    if(dyn_costs_tab.at(i-1).at(min) > dyn_costs_tab.at(i-1).at(j+1))
                        min = j+1;
                }

                //stockage de la valeur distance + min distance (j-1,j,j+1) dans tableau des couts
                dyn_costs_tab.at(i).at(j) = distances->get(i,j) + dyn_costs_tab.at(i-1).at(min);
                //stockage de la coupe optimale du minimum dans la coupe optimale aux coord(i,j)
                dyn_coupes_tab.at(i).at(j).insert(
                    dyn_coupes_tab.at(i).at(j).end(),
                    dyn_coupes_tab.at(i-1).at(min).begin(),
                    dyn_coupes_tab.at(i-1).at(min).end()
                );
            }
        }
    }


    //recherche du cout minimum dans à la ligne hauteur-1 qui sera le résultat à envoyer
    min = 0;
    for(int j = 1 ; j < largeur; ++j)
        if(dyn_costs_tab.at(hauteur-1).at(j) < dyn_costs_tab.at(hauteur-1).at(min))
            min = j;


    reverse(dyn_coupes_tab.at(hauteur-1).at(min).begin(),dyn_coupes_tab.at(hauteur-1).at(min).end());
    //stockage de la coupe optimale du cout minimum dans le paramètre de retour
    coupe = new int[hauteur];

    //retour du cout minimal
    return dyn_costs_tab.at(hauteur-1).at(min);
}

RaccordeurIteratif::~RaccordeurIteratif()
{
  // pas de ressources a liberer
}
